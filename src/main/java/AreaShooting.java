import java.util.Random;
import java.util.Scanner;

public class AreaShooting {

    public static void main(String[] args) {
        int fieldSize = 5;
        char[][] field = new char[fieldSize][fieldSize];

        for (int i = 0; i < fieldSize; i++) {
            for (int k = 0; k < fieldSize; k++) {
                field[i][k] = '-';
            }
        }
        Random random = new Random();
        int randomRow = random.nextInt(fieldSize);
        int randomCol = random.nextInt(fieldSize);

        System.out.println("All Set. Get ready to rumble!");

        Scanner userChoice = new Scanner(System.in);

        while (true) {
            printField(field);
            System.out.print("Enter row for shooting: ");
            int row = userChoice.nextInt() - 1;

            System.out.print("Enter column for shooting: ");
            int col = userChoice.nextInt() - 1;

            if (row >= 0 && row < fieldSize && col >= 0 && col < fieldSize) {
                if (row == randomRow && col == randomCol) {
                    field[row][col] = 'x';
                    printField(field);
                    System.out.println("You have won!");
                    break;
                } else {
                    field[row][col] = '*';
                    System.out.println("Miss! Try again!");
                }
            }else{
                System.out.println("Incorrectly entered coordinates. Try again");
            }
        } userChoice.close();
    }

    public static void printField(char[][] field) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < field.length; i++) {
            System.out.print((i + 1) + " | ");
            for (int k = 0; k < field[i].length; k++) {
                System.out.print(field[i][k] + " | ");
            }
            System.out.println();
        }
    }
}



